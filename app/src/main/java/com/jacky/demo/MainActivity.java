package com.jacky.demo;

import android.Manifest;
import android.app.Activity;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;

import com.jacky.demo.func.EditFilterActivity;
import com.jacky.demo.func.EditTextActivity;
import com.jacky.demo.func.FingerActivity;
import com.jacky.demo.func.KillBackgroundActivity;
import com.jacky.demo.func.LoopViewPagerActivity;
import com.jacky.demo.func.Record2Activity;
import com.jacky.demo.func.RecordActivity;
import com.jacky.demo.func.TxWaterMaskActivity;
import com.jacky.demo.func.WebViewActivity;
import com.jacky.demo.ui.Camera2Activity;
import com.jacky.demo.ui.LoopViewActivity;
import com.jacky.demo.ui.PagerTabActivity;
import com.jacky.demo.ui.PasswordInputActivity;
import com.jacky.demo.ui.PlumActivity;
import com.jacky.demo.ui.PreviewActivity;
import com.jacky.demo.ui.ScrollViewActivity;
import com.jacky.demo.ui.SeekBarActivity;
import com.jacky.demo.ui.SwipeItemActivity;
import com.jacky.demo.ui.TextViewActivity;
import com.jacky.demo.ui.VerticalSeekBarActivity;
import com.jacky.demo.util.AppUtils;
import com.jacky.demo.util.ViewUtils;

/**
 * Created by Administrator on 2017-06-12.
 */

public class MainActivity extends BaseActivity implements View.OnClickListener {

    @Override
    protected int getContentView() {
        return R.layout.main_layout;
    }

    @Override
    protected void initCreate() {
        setOnClick(R.id.flowlayout_ui);
        setOnClick(R.id.flowlayout_func);
//        startActivity(Record2Activity.class);

        AppUtils.requestPermission(this, 1, Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(!AppUtils.isPermissionOK(this, permissions, grantResults)) {
            finish();
        }
    }

    private void setOnClick(int id) {
        ViewGroup group = (ViewGroup) findViewById(id);
        int size = group.getChildCount();
        for(int i = 0;i < size;i++) {
            group.getChildAt(i).setOnClickListener(this);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.vertical_seek : startActivity(VerticalSeekBarActivity.class); break;
            case R.id.scrollView : startActivity(ScrollViewActivity.class); break;
            case R.id.seekbar : startActivity(SeekBarActivity.class); break;
            case R.id.edit_text : startActivity(EditTextActivity.class); break;
            case R.id.pager_tab : startActivity(PagerTabActivity.class); break;
            case R.id.finger : startActivity(FingerActivity.class); break;
            case R.id.plum_layout : startActivity(PlumActivity.class); break;
            case R.id.special_textview : startActivity(TextViewActivity.class); break;
            case R.id.loop_view : startActivity(LoopViewActivity.class); break;
            case R.id.edit_filter : startActivity(EditFilterActivity.class); break;
            case R.id.loop_view_pager : startActivity(LoopViewPagerActivity.class); break;
            case R.id.password_input : startActivity(PasswordInputActivity.class); break;
            case R.id.webview : startActivity(WebViewActivity.class);  break;
            case R.id.record : startActivity(RecordActivity.class); break;
            case R.id.record2 : startActivity(Record2Activity.class); break;
            case R.id.kill_bg : startActivity(KillBackgroundActivity.class); break;
            case R.id.preview_camera : startActivity(PreviewActivity.class); break;
            case R.id.swipe_item : startActivity(SwipeItemActivity.class); break;
            case R.id.camera2 : startActivity(Camera2Activity.class); break;
//            case R.id.big_image : startActivity(LargeImageActivity.class); break;
            case R.id.tx_water : startActivity(TxWaterMaskActivity.class); break;
        }
    }
    
    private void startActivity(Class<? extends Activity> clazz) {
        ViewUtils.startActivity(this, clazz);
    }
}
