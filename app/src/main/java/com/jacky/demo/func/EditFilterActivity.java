package com.jacky.demo.func;

import android.text.InputFilter;
import android.widget.EditText;

import com.jacky.demo.BaseActivity;
import com.jacky.demo.R;
import com.jacky.demo.util.InputFilterExtra;
import com.jacky.demo.util.ViewUtils;

/**
 * Created by Administrator on 2017-08-30.
 */

public class EditFilterActivity extends BaseActivity {

    @Override
    protected int getContentView() {
        return R.layout.edit_filter_activity;
    }

    @Override
    protected void initCreate() {
        setTitle("EditText Filter");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        EditText text = ViewUtils.findView(this, R.id.name);
        text.setFilters(new InputFilter[]{new InputFilterExtra.ChineseFilter()});

        EditText card = ViewUtils.findView(this, R.id.card);
        card.setFilters(new InputFilter[]{new InputFilterExtra.CardIDFilter()});

        EditText length = ViewUtils.findView(this, R.id.length);
        length.setFilters(new InputFilter[]{new InputFilterExtra.LengthInputFilter(10)});

        EditText rate = ViewUtils.findView(this, R.id.rate);
        rate.setFilters(new InputFilter[]{new InputFilterExtra.DecimalInputFilter(4, 3, true)});

        EditText emoji = ViewUtils.findView(this, R.id.emoji);
        emoji.setFilters(new InputFilter[]{new InputFilterExtra.EmojiInputFilter()});
    }
}
