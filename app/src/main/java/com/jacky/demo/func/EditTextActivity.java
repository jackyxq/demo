package com.jacky.demo.func;

import android.text.Editable;
import android.text.Selection;
import android.text.TextWatcher;
import android.widget.EditText;

import com.jacky.demo.BaseActivity;
import com.jacky.demo.R;

/**
 * Created by Administrator on 2017-07-20.
 */

public class EditTextActivity extends BaseActivity {

    @Override
    protected int getContentView() {
        return R.layout.edit_text_activity;
    }

    @Override
    protected void initCreate() {
        setTitle("输入框格式化");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        EditText phone = (EditText) findViewById(R.id.phone);
        phone.addTextChangedListener(new PhoneTextWatcher());

        EditText bank = (EditText) findViewById(R.id.bank_no);
        bank.addTextChangedListener(new NumberTextWatcher(4, 20));
    }

    private class NumberTextWatcher implements TextWatcher {

        private int num;
        private boolean isChange;
        private int position;
        private int maxLength;

        public NumberTextWatcher(int n) {
            this(n, Integer.MAX_VALUE);
        }

        public NumberTextWatcher(int n, int maxLength) {
            this.num = n;
            this.maxLength = maxLength;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if(isChange) return;
            position = start + 1;
        }

        @Override
        public void afterTextChanged(Editable s) {
            if(isChange) {
                isChange = false;
                return;
            }
            String oldString = s.toString();
            int len = s.length();

            StringBuilder sb = new StringBuilder();
            for(int i = 0;i < len;i++) {
                char c = oldString.charAt(i);
                if(c == ' ') {
                    position--;
                    continue;
                }
                if(sb.length() % (num + 1) == num) {
                    sb.append(' ');
                    position++;
                }
                sb.append(c);
            }

            String newString = sb.toString();
            if(!oldString.equals(newString)) {
                isChange = true;
                s.replace(0, len, newString);
                len = Math.min(newString.length(), maxLength);
                Selection.setSelection(s, Math.min(len, position));
            }
        }
    }


    private class PhoneTextWatcher implements TextWatcher {

        private boolean isChange;
        private int position;

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if(isChange) return;
            position = start + 1;
        }

        @Override
        public void afterTextChanged(Editable s) {
            if(isChange) {
                isChange = false;
                return;
            }
            String oldString = s.toString();
            int len = s.length();

            StringBuilder sb = new StringBuilder();
            for(int i = 0;i < len;i++) {
                char c = oldString.charAt(i);
                if(c == ' ') {
                    position--;
                    continue;
                }
                if(sb.length() == 3 || sb.length() == 8) {
                    sb.append(' ');
                    position++;
                }
                sb.append(c);
            }

            String newString = sb.toString();
            if(!oldString.equals(newString)) {
                isChange = true;
                s.replace(0, len, newString);
                len = newString.length();
                Selection.setSelection(s, position > len ? len : position);
            }
        }
    }
}
