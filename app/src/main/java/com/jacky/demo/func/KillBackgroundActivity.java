package com.jacky.demo.func;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Build;
import android.view.View;
import android.widget.ImageView;

import com.jacky.demo.BaseActivity;
import com.jacky.demo.R;
import com.jacky.demo.util.ViewUtils;
import com.jacky.log.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.CountDownLatch;

/**
 * Created by jacky on 2018/5/11.
 */

public class KillBackgroundActivity extends BaseActivity implements View.OnClickListener{

    ImageView imageView;
    Bitmap mBitmap;

    int[] allpixels;
    int[] bidui;
    CountDownLatch mLatch;
    long start;

    @Override
    protected int getContentView() {
        return R.layout.kill_bg_activity;
    }

    @Override
    protected void initCreate() {
            imageView = ViewUtils.findView(this, R.id.image);
            ViewUtils.findView(this, R.id.btn_gray).setOnClickListener(this);

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("图片抠底");

        try {
            InputStream stream = getAssets().open("3.jpg");
            Bitmap bm = BitmapFactory.decodeStream(stream);
            mBitmap = bm.copy(Bitmap.Config.ARGB_8888, true);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                mBitmap.setConfig(Bitmap.Config.ARGB_8888);
            }
            bm.recycle();
            imageView.setImageBitmap(mBitmap);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_gray : grayBitmap(); break;
        }
    }

    private void grayBitmap() {
        Logger.d("start pixel");
         start = System.currentTimeMillis();
        int width = mBitmap.getWidth(), height = mBitmap.getHeight();
        allpixels = new int[width * height];
         mBitmap.getPixels(allpixels, 0, width, 0, 0, width, height);
        Logger.d("start gray");
         bidui = new int[100];
        for(int i = 0;i < bidui.length;i++) {
                bidui[i] = getGrayColor(mBitmap.getPixel(i/10, i % 10));
        }
        Logger.d("start gray2", Runtime.getRuntime().availableProcessors());
        //1-----4950
        //2----2539
        //3----2818
        //4----2072
        //5----1931
        //6----1630
        //7----1423
        //8----1293
        //9----1153
        //10---1215
        //11---1044
        //12---1049
        //16---1086
        int size = 8;
        int fragment = allpixels.length / size;
        mLatch = new CountDownLatch(size);
        Logger.e(allpixels.length);
        for(int i = 1;i <= size;i++) {
            int end = (i == size ? allpixels.length :(i * fragment)) - 1;
            new Thread(new RepalceRunnable((i - 1) * fragment, end)).start();
        }
    }

    private boolean findInBIdui(int color) {
        if(color == 0) return false;
        for(int i = 0;i < bidui.length;i++) {
                if(Math.abs(bidui[i] - color) < 10) return true;
        }
        return false;
    }

    private int getGrayColor(int color) {
        if(color == 0) return 0;
        int red = Color.red(color);
        int green = Color.green(color);
        int blue = Color.blue(color);

        return (int) (0.3f * red + 0.59f * green + 0.11f * blue);
    }

    private class RepalceRunnable implements Runnable {

        int startPixel, endPixel;
        RepalceRunnable(int start, int end) {
            startPixel = start;
            endPixel = end;
            Logger.w(startPixel, endPixel);
        }

        @Override
        public void run() {
                for(int j = startPixel;j <= endPixel;j++) {
                    if(findInBIdui(getGrayColor(allpixels[j]))) {
                        allpixels[j] = 0;
                    }
                }
            mLatch.countDown();
            if(mLatch.getCount() <= 0) {
                long end = System.currentTimeMillis();
                Logger.i("end gray", (end - start)); //1324 ms
                int width = mBitmap.getWidth(), height = mBitmap.getHeight();
                mBitmap.setPixels(allpixels, 0 , width, 0,0 , width, height);
                allpixels = null;
                Logger.i("end gray2....."); //13083 ms
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        imageView.setImageBitmap(mBitmap);
                    }
                });
            }
        }
    }
}
