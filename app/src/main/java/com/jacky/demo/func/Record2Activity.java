package com.jacky.demo.func;

import android.Manifest;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.view.View;

import com.jacky.demo.BaseActivity;
import com.jacky.demo.R;
import com.jacky.demo.opengl.CameraGLSurfaceView;
import com.jacky.demo.opengl.MediaRecorder;
import com.jacky.demo.util.AppUtils;
import com.jacky.log.Logger;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;


/**
 * Created by lixinquan on 2018/6/19.
 */
public class Record2Activity extends BaseActivity implements View.OnClickListener {

    CameraGLSurfaceView mSurfaceView;

    MediaRecorder mediaRecorder;

    View startView, endView;
    boolean slience = false;

    @Override
    protected int getContentView() {
        return R.layout.record2_activity;
    }

    @Override
    protected void initCreate() {
        setTitle("GLSurfaceView");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().hide();
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        mSurfaceView =  findViewById(R.id.surface);
        startView = findViewById(R.id.start);
        startView.setOnClickListener(this);
        endView = findViewById(R.id.stop);
        endView.setOnClickListener(this);
        findViewById(R.id.switch_camera).setOnClickListener(this);
        findViewById(R.id.btn_take_picture).setOnClickListener(this);
        findViewById(R.id.btn_slience).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.start :
                AppUtils.requestPermission(this, 1,
                        Manifest.permission.CAMERA,
                        Manifest.permission.RECORD_AUDIO,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE);
                break;
            case R.id.stop :
                stop();
                break;
            case R.id.switch_camera :
                merge();
                break;
            case R.id.btn_take_picture :
                mSurfaceView.takePicture(new CameraGLSurfaceView.PictureCallback() {
                    @Override
                    public void onPictureTaken(Bitmap bitmap) {
                        Logger.w(bitmap.getWidth(), bitmap.getHeight());

                        FileOutputStream outputStream = null;
                        try {
                            outputStream = new FileOutputStream("/sdcard/360/tttt.png");
                            bitmap.compress(Bitmap.CompressFormat.JPEG, 80, outputStream);
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }
                    }
                });
                break;
            case R.id.btn_slience :
                if(slience) {
                    slience = false;
                    mediaRecorder.turnOnSound();
                } else {
                    slience = true;
                    mediaRecorder.turnOffSound();
                }
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mSurfaceView.release();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode == 1) {
            if(AppUtils.isPermissionOK(this, permissions, grantResults)) {
                start();
            } else {
                showToast("摄像头、录音或者存储权限未开启");
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mSurfaceView.resumeRecord();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mSurfaceView.pauseRecord();
    }

    long starttime;
    private void start() {
        mediaRecorder = new MediaRecorder("/sdcard/360/testttt.mp4")
                .setAudioEncoder(android.media.MediaRecorder.AudioSource.MIC)
                .setPreviewHolder(mSurfaceView)
                .setVideoSize(1080, 1920);

        startView.setEnabled(false);
        mediaRecorder.start();
        starttime = System.currentTimeMillis();
    }

    private void stop() {
        startView.setEnabled(true);
        mediaRecorder.stop();
        long end = System.currentTimeMillis();
        Logger.e("i want stop record", end - starttime);
        finish();
    }

    private void merge() {
        mSurfaceView.switchCamera();
    }
}

