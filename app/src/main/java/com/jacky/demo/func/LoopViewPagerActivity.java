package com.jacky.demo.func;

import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jacky.demo.BaseActivity;
import com.jacky.demo.R;
import com.jacky.demo.util.ViewUtils;
import com.jacky.demo.widget.LoopPagerAdapter;
import com.jacky.demo.widget.LoopViewPager;

import java.util.Arrays;

/**
 * Created by Administrator on 2017-09-22.
 */

public class LoopViewPagerActivity extends BaseActivity {

    @Override
    protected int getContentView() {
        return R.layout.loop_view_pager;
    }

    @Override
    protected void initCreate() {
        setTitle("LoopViewPager");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        LoopAdapter adapter = new LoopAdapter(true);
        LoopViewPager pager = ViewUtils.findView(this, R.id.loop);
        pager.setAdapter(adapter);
        adapter.setData(Arrays.asList("1", "2", "3", "4", "5"));
        pager.notifyDataSetChanged();

        adapter = new LoopAdapter(false);
        adapter.setData(Arrays.asList("1", "2", "3", "4", "5"));
        LoopViewPager pager2 = ViewUtils.findView(this, R.id.loop2);
        pager2.setAdapter(adapter);
        pager2.notifyDataSetChanged();
    }

    private class LoopAdapter extends LoopPagerAdapter<String> {

        LoopAdapter(boolean loop) {
            super(loop);
        }

        @Override
        public View getItemView(ViewGroup container, int position) {
            TextView view = new TextView(getActivity());
            String s = getItem(position);
            view.setText(s);
            switch (s) {
                case "5" :
                    view.setBackgroundColor(0xffff0000);
                    break;
                case "1" :
                    view.setBackgroundColor(0xff00ff00);
                    break;
                case "2" :
                    view.setBackgroundColor(0xff0000ff);
                    break;
                case "3" :
                    view.setBackgroundColor(0xffffff00);
                    break;
                case "4" :
                    view.setBackgroundColor(0xff00ffff);
                    break;
            }
            return  view;
        }
    }
}
