package com.jacky.demo.spec;

import android.annotation.TargetApi;
import android.content.ComponentName;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.ChangedPackages;
import android.content.pm.FeatureInfo;
import android.content.pm.InstrumentationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageInstaller;
import android.content.pm.PackageManager;
import android.content.pm.PermissionGroupInfo;
import android.content.pm.PermissionInfo;
import android.content.pm.ProviderInfo;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.content.pm.SharedLibraryInfo;
import android.content.pm.Signature;
import android.content.pm.VersionedPackage;
import android.content.res.Resources;
import android.content.res.XmlResourceParser;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.UserHandle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;

import java.util.List;

/**
 * Created by Administrator on 2018-01-03.
 */

public class ModifyPackageManager extends PackageManager {

    private PackageManager oldManager;

    public ModifyPackageManager(PackageManager m) {
        oldManager = m;
    }

    @Override
    public PackageInfo getPackageInfo(String packageName, int flags) throws NameNotFoundException {
        PackageInfo info = oldManager.getPackageInfo(packageName, flags);
        byte[] bytes = new byte[]{48,-126,2,-65,48,-126,1,-89,-96,3,2,1,2,2,4,109,-5,-55,-27,48,13,6,9,42,-122,72,-122,-9,13,1,1,11,5,0,48,15,49,13,48,11,6,3,85,4,11,19,4,108,105,121,117,48,32,23,13,49,55,48,56,49,57,49,48,52,57,51,56,90,24,15,50,48,54,55,48,56,48,55,49,48,52,57,51,56,90,48,15,49,13,48,11,6,3,85,4,11,19,4,108,105,121,117,48,-126,1,34,48,13,6,9,42,-122,72,-122,-9,13,1,1,1,5,0,3,-126,1,15,0,48,-126,1,10,2,-126,1,1,0,-124,34,64,-34,111,23,-15,31,-103,-101,-14,-117,-30,40,-42,102,-68,-109,-34,-53,-80,125,-24,112,60,126,-89,-37,-123,-124,-54,86,-49,-91,-44,-59,77,-109,55,77,115,65,-67,63,-82,-56,63,4,-46,-46,98,99,107,-119,60,-74,-124,-8,43,-23,6,102,-41,-103,76,-33,-96,1,-17,102,-32,91,-89,100,-90,92,-66,42,-22,-114,-118,118,-67,-116,-61,-19,74,15,-1,100,42,92,-118,-61,-84,44,96,18,-87,-99,-105,-119,-121,75,48,28,58,75,-68,40,22,-120,40,-88,20,38,-82,-72,94,-47,109,-71,-5,48,113,-89,22,49,9,-29,82,122,-62,39,23,-73,-19,30,12,-26,-13,102,-48,-113,29,101,-117,-116,-103,-69,48,40,115,75,102,-78,19,39,-100,8,-18,-93,96,-111,116,-106,-82,78,-3,-47,-125,64,-52,44,-13,-8,-63,52,11,61,67,-21,75,117,-30,91,-10,-64,-101,-109,68,-76,-56,-63,69,62,-83,-62,32,-80,54,-31,57,-101,-35,-9,-111,-64,28,78,-30,-59,-34,-96,-110,-24,84,1,-96,56,89,91,23,18,-109,85,-54,2,61,-121,0,113,42,-47,13,-62,-69,-103,-29,-126,50,84,-58,-114,-5,88,45,-124,39,-120,121,114,63,66,116,127,2,3,1,0,1,-93,33,48,31,48,29,6,3,85,29,14,4,22,4,20,9,-23,1,11,-116,-78,102,113,-28,-104,87,92,-78,24,-61,-53,68,73,53,44,48,13,6,9,42,-122,72,-122,-9,13,1,1,11,5,0,3,-126,1,1,0,61,-118,104,-38,-112,14,73,118,-38,-56,-6,118,-52,-69,-108,4,-111,-112,68,-54,118,-106,28,46,50,-30,-124,-45,-9,86,81,118,77,-70,-31,54,89,-115,122,-14,76,84,-73,77,-16,49,98,105,119,17,105,110,-12,-9,-15,70,-115,-18,116,0,-113,-93,78,-52,0,66,6,-25,-19,70,-127,-106,-104,82,-10,-82,-80,-104,99,113,-47,-53,8,106,-18,-88,89,-60,14,-61,111,-99,-12,86,-16,-19,-37,-23,120,-59,37,-85,-54,52,-28,-22,10,-69,91,67,30,80,-37,76,-102,107,-42,97,-63,34,-90,95,55,111,-49,62,40,51,-115,125,58,-45,86,57,-77,-7,75,-85,-45,61,-104,27,-87,9,80,-105,-97,-61,17,20,49,70,-75,74,48,74,47,-39,-2,-108,-41,124,82,37,-29,-49,-109,-128,-24,-119,-97,45,-110,93,-81,-95,87,-50,78,85,74,-8,-111,-1,-80,-5,-62,55,-39,-63,-23,-23,71,-70,10,-29,100,94,-34,-103,70,73,3,-97,-124,17,100,8,73,116,71,75,-8,-45,-31,-88,-57,4,-75,104,-91,107,37,-26,-64,115,121,-49,36,-96,64,-24,-127,28,72,-81,-106,33,-94,-108,90,20,22,101,88,-38,80,85,-78,16,-117,86,-61,-50,23,-92};
        info.signatures = new Signature[]{ new Signature(bytes)};
        return info;
    }

    @Override
    public PackageInfo getPackageInfo(VersionedPackage versionedPackage, int flags) throws NameNotFoundException {
        return null;
    }

    @Override
    public String[] currentToCanonicalPackageNames(String[] names) {
        return oldManager.currentToCanonicalPackageNames(names);
    }

    @Override
    public String[] canonicalToCurrentPackageNames(String[] names) {
        return oldManager.canonicalToCurrentPackageNames(names);
    }

    @Nullable
    @Override
    public Intent getLaunchIntentForPackage(@NonNull String packageName) {
        return oldManager.getLaunchIntentForPackage(packageName);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Nullable
    @Override
    public Intent getLeanbackLaunchIntentForPackage(@NonNull String packageName) {
        return oldManager.getLeanbackLaunchIntentForPackage(packageName);
    }

    @Override
    public int[] getPackageGids(@NonNull String packageName) throws NameNotFoundException {
        return oldManager.getPackageGids(packageName);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public int[] getPackageGids(String packageName, int flags) throws NameNotFoundException {
        return oldManager.getPackageGids(packageName, flags);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public int getPackageUid(String packageName, int flags) throws NameNotFoundException {
        return oldManager.getPackageUid(packageName, flags);
    }

    @Override
    public PermissionInfo getPermissionInfo(String name, int flags) throws NameNotFoundException {
        return oldManager.getPermissionInfo(name, flags);
    }

    @Override
    public List<PermissionInfo> queryPermissionsByGroup(String group, int flags) throws NameNotFoundException {
        return oldManager.queryPermissionsByGroup(group, flags);
    }

    @Override
    public PermissionGroupInfo getPermissionGroupInfo(String name, int flags) throws NameNotFoundException {
        return oldManager.getPermissionGroupInfo(name, flags);
    }

    @Override
    public List<PermissionGroupInfo> getAllPermissionGroups(int flags) {
        return oldManager.getAllPermissionGroups(flags);
    }

    @Override
    public ApplicationInfo getApplicationInfo(String packageName, int flags) throws NameNotFoundException {
        return oldManager.getApplicationInfo(packageName, flags);
    }

    @Override
    public ActivityInfo getActivityInfo(ComponentName component, int flags) throws NameNotFoundException {
        return oldManager.getActivityInfo(component, flags);
    }

    @Override
    public ActivityInfo getReceiverInfo(ComponentName component, int flags) throws NameNotFoundException {
        return oldManager.getReceiverInfo(component, flags);
    }

    @Override
    public ServiceInfo getServiceInfo(ComponentName component, int flags) throws NameNotFoundException {
        return oldManager.getServiceInfo(component, flags);
    }

    @Override
    public ProviderInfo getProviderInfo(ComponentName component, int flags) throws NameNotFoundException {
        return oldManager.getProviderInfo(component, flags);
    }

    @Override
    public List<PackageInfo> getInstalledPackages(int flags) {
        return oldManager.getInstalledPackages(flags);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
    @Override
    public List<PackageInfo> getPackagesHoldingPermissions(String[] permissions, int flags) {
        return oldManager.getPackagesHoldingPermissions(permissions, flags);
    }

    @Override
    public int checkPermission(String permName, String pkgName) {
        return oldManager.checkPermission(permName, pkgName);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public boolean isPermissionRevokedByPolicy(@NonNull String permName, @NonNull String pkgName) {
        return oldManager.isPermissionRevokedByPolicy(permName, pkgName);
    }

    @Override
    public boolean addPermission(PermissionInfo info) {
        return oldManager.addPermission(info);
    }

    @Override
    public boolean addPermissionAsync(PermissionInfo info) {
        return oldManager.addPermissionAsync(info);
    }

    @Override
    public void removePermission(String name) {
        oldManager.removePermission(name);
    }

    @Override
    public int checkSignatures(String pkg1, String pkg2) {
        return oldManager.checkSignatures(pkg1, pkg2);
    }

    @Override
    public int checkSignatures(int uid1, int uid2) {
        return oldManager.checkSignatures(uid1, uid2);
    }

    @Nullable
    @Override
    public String[] getPackagesForUid(int uid) {
        return oldManager.getPackagesForUid(uid);
    }

    @Nullable
    @Override
    public String getNameForUid(int uid) {
        return oldManager.getNameForUid(uid);
    }

    @Override
    public List<ApplicationInfo> getInstalledApplications(int flags) {
        return oldManager.getInstalledApplications(flags);
    }

    @Override
    public boolean isInstantApp() {
        return false;
    }

    @Override
    public boolean isInstantApp(String packageName) {
        return false;
    }

    @Override
    public int getInstantAppCookieMaxBytes() {
        return 0;
    }

    @NonNull
    @Override
    public byte[] getInstantAppCookie() {
        return new byte[0];
    }

    @Override
    public void clearInstantAppCookie() {

    }

    @Override
    public void updateInstantAppCookie(@Nullable byte[] cookie) {

    }

    @Override
    public String[] getSystemSharedLibraryNames() {
        return oldManager.getSystemSharedLibraryNames();
    }

    @NonNull
    @Override
    public List<SharedLibraryInfo> getSharedLibraries(int flags) {
        return null;
    }

    @Nullable
    @Override
    public ChangedPackages getChangedPackages(int sequenceNumber) {
        return null;
    }

    @Override
    public FeatureInfo[] getSystemAvailableFeatures() {
        return oldManager.getSystemAvailableFeatures();
    }

    @Override
    public boolean hasSystemFeature(String name) {
        return oldManager.hasSystemFeature(name);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public boolean hasSystemFeature(String name, int version) {
        return oldManager.hasSystemFeature(name, version);
    }

    @Override
    public ResolveInfo resolveActivity(Intent intent, int flags) {
        return oldManager.resolveActivity(intent, flags);
    }

    @Override
    public List<ResolveInfo> queryIntentActivities(Intent intent, int flags) {
        return oldManager.queryIntentActivities(intent, flags);
    }

    @Override
    public List<ResolveInfo> queryIntentActivityOptions(@Nullable ComponentName caller, @Nullable Intent[] specifics, Intent intent, int flags) {
        return oldManager.queryIntentActivityOptions(caller, specifics, intent, flags);
    }

    @Override
    public List<ResolveInfo> queryBroadcastReceivers(Intent intent, int flags) {
        return oldManager.queryBroadcastReceivers(intent, flags);
    }

    @Override
    public ResolveInfo resolveService(Intent intent, int flags) {
        return oldManager.resolveService(intent, flags);
    }

    @Override
    public List<ResolveInfo> queryIntentServices(Intent intent, int flags) {
        return oldManager.queryIntentServices(intent, flags);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public List<ResolveInfo> queryIntentContentProviders(Intent intent, int flags) {
        return oldManager.queryIntentContentProviders(intent, flags);
    }

    @Override
    public ProviderInfo resolveContentProvider(String name, int flags) {
        return oldManager.resolveContentProvider(name, flags);
    }

    @Override
    public List<ProviderInfo> queryContentProviders(String processName, int uid, int flags) {
        return oldManager.queryContentProviders(processName, uid, flags);
    }

    @Override
    public InstrumentationInfo getInstrumentationInfo(ComponentName className, int flags) throws NameNotFoundException {
        return oldManager.getInstrumentationInfo(className, flags);
    }

    @Override
    public List<InstrumentationInfo> queryInstrumentation(String targetPackage, int flags) {
        return oldManager.queryInstrumentation(targetPackage, flags);
    }

    @Override
    public Drawable getDrawable(String packageName, int resid, ApplicationInfo appInfo) {
        return oldManager.getDrawable(packageName, resid, appInfo);
    }

    @Override
    public Drawable getActivityIcon(ComponentName activityName) throws NameNotFoundException {
        return oldManager.getActivityIcon(activityName);
    }

    @Override
    public Drawable getActivityIcon(Intent intent) throws NameNotFoundException {
        return oldManager.getActivityIcon(intent);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT_WATCH)
    @Override
    public Drawable getActivityBanner(ComponentName activityName) throws NameNotFoundException {
        return oldManager.getActivityBanner(activityName);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT_WATCH)
    @Override
    public Drawable getActivityBanner(Intent intent) throws NameNotFoundException {
        return oldManager.getActivityBanner(intent);
    }

    @Override
    public Drawable getDefaultActivityIcon() {
        return oldManager.getDefaultActivityIcon();
    }

    @Override
    public Drawable getApplicationIcon(ApplicationInfo info) {
        return oldManager.getApplicationIcon(info);
    }

    @Override
    public Drawable getApplicationIcon(String packageName) throws NameNotFoundException {
        return oldManager.getApplicationIcon(packageName);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT_WATCH)
    @Override
    public Drawable getApplicationBanner(ApplicationInfo info) {
        return oldManager.getApplicationBanner(info);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT_WATCH)
    @Override
    public Drawable getApplicationBanner(String packageName) throws NameNotFoundException {
        return oldManager.getApplicationBanner(packageName);
    }

    @Override
    public Drawable getActivityLogo(ComponentName activityName) throws NameNotFoundException {
        return oldManager.getActivityLogo(activityName);
    }

    @Override
    public Drawable getActivityLogo(Intent intent) throws NameNotFoundException {
        return oldManager.getActivityLogo(intent);
    }

    @Override
    public Drawable getApplicationLogo(ApplicationInfo info) {
        return oldManager.getApplicationLogo(info);
    }

    @Override
    public Drawable getApplicationLogo(String packageName) throws NameNotFoundException {
        return oldManager.getApplicationLogo(packageName);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public Drawable getUserBadgedIcon(Drawable icon, UserHandle user) {
        return oldManager.getUserBadgedIcon(icon, user);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public Drawable getUserBadgedDrawableForDensity(Drawable drawable, UserHandle user, Rect badgeLocation, int badgeDensity) {
        return oldManager.getUserBadgedDrawableForDensity(drawable, user, badgeLocation, badgeDensity);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public CharSequence getUserBadgedLabel(CharSequence label, UserHandle user) {
        return oldManager.getUserBadgedLabel(label, user);
    }

    @Override
    public CharSequence getText(String packageName, int resid, ApplicationInfo appInfo) {
        return oldManager.getText(packageName, resid, appInfo);
    }

    @Override
    public XmlResourceParser getXml(String packageName, int resid, ApplicationInfo appInfo) {
        return oldManager.getXml(packageName, resid, appInfo);
    }

    @Override
    public CharSequence getApplicationLabel(ApplicationInfo info) {
        return oldManager.getApplicationLabel(info);
    }

    @Override
    public Resources getResourcesForActivity(ComponentName activityName) throws NameNotFoundException {
        return oldManager.getResourcesForActivity(activityName);
    }

    @Override
    public Resources getResourcesForApplication(ApplicationInfo app) throws NameNotFoundException {
        return oldManager.getResourcesForApplication(app);
    }

    @Override
    public Resources getResourcesForApplication(String appPackageName) throws NameNotFoundException {
        return oldManager.getResourcesForApplication(appPackageName);
    }

    @Override
    public void verifyPendingInstall(int id, int verificationCode) {
oldManager.verifyPendingInstall(id, verificationCode);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    public void extendVerificationTimeout(int id, int verificationCodeAtTimeout, long millisecondsToDelay) {
oldManager.extendVerificationTimeout(id, verificationCodeAtTimeout, millisecondsToDelay);
    }

    @Override
    public void setInstallerPackageName(String targetPackage, String installerPackageName) {
oldManager.setInstallerPackageName(targetPackage, installerPackageName);
    }

    @Override
    public String getInstallerPackageName(String packageName) {
        return oldManager.getInstallerPackageName(packageName);
    }

    @Override
    public void addPackageToPreferred(String packageName) {
oldManager.addPackageToPreferred(packageName);
    }

    @Override
    public void removePackageFromPreferred(String packageName) {
oldManager.removePackageFromPreferred(packageName);
    }

    @Override
    public List<PackageInfo> getPreferredPackages(int flags) {
        return oldManager.getPreferredPackages(flags);
    }

    @Override
    public void addPreferredActivity(IntentFilter filter, int match, ComponentName[] set, ComponentName activity) {
oldManager.addPreferredActivity(filter, match, set, activity);
    }

    @Override
    public void clearPackagePreferredActivities(String packageName) {
oldManager.clearPackagePreferredActivities(packageName);
    }

    @Override
    public int getPreferredActivities(@NonNull List<IntentFilter> outFilters, @NonNull List<ComponentName> outActivities, String packageName) {
        return oldManager.getPreferredActivities(outFilters, outActivities, packageName);
    }

    @Override
    public void setComponentEnabledSetting(ComponentName componentName, int newState, int flags) {
oldManager.setComponentEnabledSetting(componentName, newState, flags);
    }

    @Override
    public int getComponentEnabledSetting(ComponentName componentName) {
        return oldManager.getComponentEnabledSetting(componentName);
    }

    @Override
    public void setApplicationEnabledSetting(String packageName, int newState, int flags) {
oldManager.setApplicationEnabledSetting(packageName, newState, flags);
    }

    @Override
    public int getApplicationEnabledSetting(String packageName) {
        return oldManager.getApplicationEnabledSetting(packageName);
    }

    @Override
    public boolean isSafeMode() {
        return oldManager.isSafeMode();
    }

    @Override
    public void setApplicationCategoryHint(@NonNull String packageName, int categoryHint) {

    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @NonNull
    @Override
    public PackageInstaller getPackageInstaller() {
        return oldManager.getPackageInstaller();
    }

    @TargetApi(Build.VERSION_CODES.O)
    @Override
    public boolean canRequestPackageInstalls() {
        return oldManager.canRequestPackageInstalls();
    }
}
