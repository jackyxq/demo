package com.jacky.demo.spec;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.jacky.log.Logger;

/**
 * Created by jacky on 2018/5/20.
 */

public class LauncherService extends Service {

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Logger.d("start launcherService....");
    }
}
