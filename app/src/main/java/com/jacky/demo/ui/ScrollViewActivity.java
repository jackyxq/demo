package com.jacky.demo.ui;

import android.widget.ImageView;
import android.widget.TextView;

import com.jacky.demo.BaseActivity;
import com.jacky.demo.R;
import com.jacky.demo.util.ViewUtils;
import com.jacky.demo.widget.TwoScrollView;

/**
 * Created by Administrator on 2017-06-12.
 */

public class ScrollViewActivity extends BaseActivity {

    private ImageView mSwitchArrow;
    private TextView mSwitchView;

    @Override
    protected int getContentView() {
        return R.layout.scrollview_activity;
    }

    @Override
    protected void initCreate() {

        setTitle("TwoScrollView");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mSwitchArrow = ViewUtils.findView(this, R.id.switch_arrow);
        mSwitchView = ViewUtils.findView(this, R.id.switch_line);
        TwoScrollView view = ViewUtils.findView(this, R.id.scrollView);

        view.setOnScrollListener(new TwoScrollView.OnScrollListener() {
            @Override
            public void onScroll(boolean isFirst, boolean upScroll, int scrollY, int line) {
                if(isFirst) {
                    mSwitchView.setText("继续拖动，查看第二屏");
                    mSwitchArrow.setImageResource(R.mipmap.scroll_view_arrow1);
                } else if(!upScroll) {
                    mSwitchArrow.setImageResource(R.mipmap.scroll_view_arrow2);
                    mSwitchView.setText(scrollY < line ? "松开手指，返回上一屏" : "继续拖动，返回上一屏");
                }
            }
        });
    }
}
