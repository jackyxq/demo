package com.jacky.demo.ui;

import android.view.View;
import android.widget.Button;

import com.jacky.demo.BaseActivity;
import com.jacky.demo.R;
import com.jacky.demo.util.ViewUtils;
import com.jacky.demo.widget.PlumLayout;

/**
 * Created by Administrator on 2017-08-04.
 */

public class PlumActivity extends BaseActivity {

    @Override
    protected int getContentView() {
        return R.layout.plum_layout;
    }

    @Override
    protected void initCreate() {
        setTitle("梅花布局");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        PlumLayout layout = ViewUtils.findView(this, R.id.plum_layout);
        layout.setAdapter(new PlumLayout.PlumAdapter(){

            @Override
            public int getCount() {
                return 7;
            }

            @Override
            public View getCenterView(View view) {
                Button button = new Button(getBaseContext());
                button.setText("center");
                return button;
            }

            @Override
            public View getAroundView(View view, int position) {
                Button textView = new Button(getBaseContext());
                textView.setText("" + position);
                return textView;
            }
        });

        layout = ViewUtils.findView(this, R.id.plum_layout2);
        layout.setOnItemClickListener(new PlumLayout.OnItemClickListener() {

            @Override
            public void onItemClick(PlumLayout parent, View view, int position) {
                showToast("pos: " + position);
            }
        });
    }
}
