package com.jacky.demo.ui;

import com.jacky.demo.BaseActivity;
import com.jacky.demo.R;
import com.jacky.demo.widget.loop.LoopText;
import com.jacky.demo.widget.loop.LoopView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2017-08-17.
 */

public class LoopViewActivity extends BaseActivity {

    @Override
    protected int getContentView() {
        return R.layout.loop_view_activity;
    }

    @Override
    protected void initCreate() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("LoopView");

        LoopView loop = (LoopView) findViewById(R.id.loop);
        LoopView loop2 = (LoopView) findViewById(R.id.loop2);
        List<Loo> list = b("11", "22", "33", "44","55","66");
        loop.setItems(list);
        loop2.setItems(b("131", "232", "343", "454","565","676"));
        loop2.setNotLoop();
    }

    private class Loo implements LoopText {

        private String s;
        Loo(String s) {
            this.s = s;
        }

        @Override
        public String getText() {
            return s;
        }
    }

    private List<Loo> b(String... s) {
        List<Loo> list = new ArrayList<>();
        for(String dd : s) {
            list.add(new Loo(dd));
        }
        return list;
    }
}
