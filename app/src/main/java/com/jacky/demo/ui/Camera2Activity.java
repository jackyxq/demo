package com.jacky.demo.ui;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;

import com.jacky.demo.BaseActivity;
import com.jacky.demo.R;
import com.jacky.demo.widget.camera.CameraFragment;
import com.jacky.demo.widget.camera.OnImageListener;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by lixinquan on 2018/11/2.
 */
public class Camera2Activity extends BaseActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();

        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY | View.SYSTEM_UI_FLAG_FULLSCREEN;
        View view = getWindow().getDecorView();
        view.setSystemUiVisibility(uiOptions);
        view.setKeepScreenOn(true);
    }

    @Override
    protected int getContentView() {
        return R.layout.camera2_activity;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_carema_switch : mFragment.switchCamera(); break;
            case R.id.back : onBackPressed(); break;
            case R.id.flash : mFragment.toggleFlashLight(); break;
            case R.id.btn_take_picture : mFragment.takePicture(); break;
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_VOLUME_UP) {
            mFragment.toggleLock(); return true;
        } else if(keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) {
            mFragment.takePicture(); return true;
        } else if(keyCode == KeyEvent.KEYCODE_BACK) {
            if(mFragment.isScreenLock()) return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    CameraFragment mFragment;

    @SuppressLint("MissingPermission")
    @Override
    protected void initCreate() {
        getSupportActionBar().hide();
        findViewById(R.id.iv_carema_switch).setOnClickListener(this);
        findViewById(R.id.back).setOnClickListener(this);
        findViewById(R.id.btn_take_picture).setOnClickListener(this);
        findViewById(R.id.flash).setOnClickListener(this);

        mFragment = (CameraFragment) getFragmentManager().findFragmentById(R.id.surface);
        mFragment.setImageListener(new OnImageListener() {
            @Override
            public void onImage(byte[] data, int rotation) {
                File file = new File(getExternalMediaDirs()[0],
                        String.format("xjpz%s.jpg", formatTime()));
                CameraFragment.saveImage(data, rotation, file);
            }
        });
    }

    public static String formatTime() {
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd", Locale.CHINA);
        Date curDate = new Date();
        return format.format(curDate);
    }
}
