package com.jacky.demo.ui;

import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.jacky.demo.BaseActivity;
import com.jacky.demo.R;
import com.jacky.demo.util.ViewUtils;

/**
 * Created by Administrator on 2017-12-18.
 */

public class PasswordInputActivity extends BaseActivity {

    EditText mText;

    @Override
    protected int getContentView() {
        return R.layout.password_input_activity;
    }

    @Override
    protected void initCreate() {
        setTitle("仿支付宝密码输入框");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mText = ViewUtils.findView(this, R.id.password_input);
        mText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(actionId == EditorInfo.IME_ACTION_GO) {
                    showToast(v.getText().toString());
                }
                return false;
            }
        });
    }

}
