package com.jacky.demo.ui;

import android.widget.FrameLayout;

import com.jacky.demo.BaseActivity;
import com.jacky.demo.util.AppUtils;
import com.jacky.demo.widget.VerticalSeekBar;

/**
 * Created by lixinquan on 2018/9/29.
 */
public class VerticalSeekBarActivity extends BaseActivity {
    @Override
    protected int getContentView() {
        return 0;
    }

    @Override
    protected void initCreate() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("垂直滑块");

        VerticalSeekBar seekBar = new VerticalSeekBar(this);

        FrameLayout.LayoutParams p  = new FrameLayout.LayoutParams(-2, -1);
        p.leftMargin = AppUtils.dp2px(this, 50);
        seekBar.setPadding(0,0,0,p.leftMargin);
        setContentView(seekBar, p);
    }
}
