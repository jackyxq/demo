package com.jacky.demo.ui;

import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import com.jacky.demo.BaseActivity;
import com.jacky.demo.R;
import com.jacky.demo.util.ToastUtil;
import com.jacky.demo.widget.RecyclerViewAdapter;
import com.jacky.demo.widget.RecyclerViewHolder;
import com.jacky.demo.widget.SwipeItemLayout;
import com.jacky.log.Logger;

/**
 * Created by lixinquan on 2018/8/1.
 */

public class SwipeItemActivity extends BaseActivity {
    RecyclerView listView;
    @Override
    protected int getContentView() {
        return R.layout.toolbar_recyclerview;
    }

    @Override
    protected void initCreate() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("侧滑控件");

        listView = com.jacky.demo.util.ViewUtils.findView(this, R.id.recycler_view);
        listView.setLayoutManager(new LinearLayoutManager(this));
        listView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        listView.setAdapter(new ItemAdapter());
    }

    private class ItemAdapter extends RecyclerViewAdapter<String> implements View.OnClickListener {

        LayoutInflater mInflater = getLayoutInflater();
        @Override
        public View onCreateView(ViewGroup group, int viewType) {
            return mInflater.inflate(R.layout.swipe_item, group, false);
        }

        @Override
        public void onBindViewHolder(RecyclerViewHolder holder, String item, int position) {
            holder.getTextView(R.id.text).setText("" + position);
            View view;
            view = holder.getView(R.id.left);
            view.setOnClickListener(this);
            view.setTag(position);

            view = holder.getView(R.id.right);
            view.setTag(position);
            view.setOnClickListener(this);
        }

        @Override
        public int getItemCount() {
            return 30;
        }

        @Override
        public void onClick(View v) {
            TextView tv = (TextView) v;
            String s = tv.getText().toString() + v.getTag();
            Logger.d(s);
            ToastUtil.showMsg(s);
        }

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            ToastUtil.showMsg("center" + position);
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        SwipeItemLayout.resetAllSlide(listView, ev);
        return super.dispatchTouchEvent(ev);
    }
}
