package com.jacky.demo.ui;

import android.widget.SeekBar;
import android.widget.Toast;

import com.jacky.demo.BaseActivity;
import com.jacky.demo.R;
import com.jacky.demo.util.ViewUtils;

/**
 * Created by Administrator on 2017-06-21.
 */

public class SeekBarActivity extends BaseActivity {

    @Override
    protected int getContentView() {
        return R.layout.seek_bar_activity;
    }

    @Override
    protected void initCreate() {
        setTitle("SeekBar");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        SeekBar seekBar1 = ViewUtils.findView(this, R.id.seekbar1);
        SeekBar seekBar2 = ViewUtils.findView(this, R.id.seekbar2);

        SeekBar.OnSeekBarChangeListener listener = new SeekBar.OnSeekBarChangeListener() {

            Toast mToast = Toast.makeText(getActivity(), "", Toast.LENGTH_SHORT);

            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                mToast.setText(String.valueOf(i));
                mToast.show();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {}
        };

        seekBar1.setOnSeekBarChangeListener(listener);
        seekBar2.setOnSeekBarChangeListener(listener);
    }
}
