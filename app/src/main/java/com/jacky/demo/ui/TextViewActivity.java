package com.jacky.demo.ui;

import com.jacky.demo.BaseActivity;
import com.jacky.demo.R;

/**
 * Created by Administrator on 2017-08-11.
 */

public class TextViewActivity extends BaseActivity {
    @Override
    protected int getContentView() {
        return R.layout.textview_activity;
    }

    @Override
    protected void initCreate() {
        setTitle("TextView");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
}
