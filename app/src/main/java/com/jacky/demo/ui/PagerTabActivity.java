package com.jacky.demo.ui;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jacky.demo.BaseActivity;
import com.jacky.demo.R;
import com.jacky.demo.util.ViewUtils;
import com.jacky.demo.widget.TextTabLayout;

/**
 * Created by Administrator on 2017-07-25.
 */

public class PagerTabActivity extends BaseActivity {

    @Override
    protected int getContentView() {
        return R.layout.pager_tab_activity;
    }

    @Override
    protected void initCreate() {
        setTitle("PagerTab");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ViewPager pager = ViewUtils.findView(this, R.id.view_pager);
        TextTabLayout group = ViewUtils.findView(this, R.id.pager_group);
        group.setTabMode(TabLayout.MODE_SCROLLABLE);

        TextTabLayout group2 = ViewUtils.findView(this, R.id.pager_group2);
        group2.addTab(group2.newTab().setText("test1"));
        group2.addTab(group2.newTab().setText("test2"));
        group2.addTab(group2.newTab().setText("test3"));

        TabLayout tabs = ViewUtils.findView(this, R.id.tab_layout);
        tabs.setTabMode(TabLayout.MODE_SCROLLABLE);

        pager.setAdapter(new ViewPagerAdapter());

        group.setupWithViewPager(pager);
        tabs.setupWithViewPager(pager);

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.layout_container, new TestFragment());
        transaction.commit();
    }

    private class ViewPagerAdapter extends PagerAdapter {

        @Override
        public int getCount() {
            return 7;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            TextView view = new TextView(getActivity());
            view.setText("" + position);
            view.setTextSize(20);
            container.addView(view);
            return view;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0 : return "tab0";
                case 1 : return "tab1tab";
                case 2 : return "tab22tab";
                case 3 : return "tab333tab";
                case 4 : return "tab4tab";
                case 5 : return "tab5tab";
                case 6 : return "tab6tab";
            }
            return "";
        }
    }

    public static class TestFragment extends Fragment {
        @Nullable
        @Override
        public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            TextTabLayout layout = new TextTabLayout(container.getContext());
            return layout;
        }

        @Override
        public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
            TextTabLayout layout = (TextTabLayout) view;
            layout.addTab(layout.newTab().setText("fragment1"));
            layout.addTab(layout.newTab().setText("fragment2"));
            layout.addTab(layout.newTab().setText("fragment3"));
        }
    }
}
