package com.jacky.demo;

import android.app.Application;
import android.content.Context;

import com.jacky.demo.spec.HookHelper;
import com.jacky.demo.util.StorageUtil;

/**
 * Created by Administrator on 2017-06-27.
 */

public class DemoApp extends Application {

    private static DemoApp mApp;

    @Override
    public void onCreate() {
        super.onCreate();

        mApp = this;
        StorageUtil.initialize();
        ErrorActivity.setDefaultUncaughtExceptionHandler(this);
    }

    public static DemoApp get() {
        return mApp;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        HookHelper.attachContext();
    }
}
