package com.jacky.demo.widget;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ImageFormat;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.hardware.Camera;
import android.provider.Settings;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.SeekBar;
import android.widget.Toast;

import com.jacky.demo.util.AppUtils;
import com.jacky.log.Logger;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by lixinquan on 2018/5/25.
 */

public class CameraPreviewView extends FrameLayout {
    public CameraPreviewView(@NonNull Context context) {
        super(context);
        initView(context, null);
    }
    public CameraPreviewView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView(context, attrs);
    }
    public CameraPreviewView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context, attrs);
    }

    private static final int SHOW_TIME = 1000;

    CameraView mInnerView;
    TouchDrawView mFocusView;
    SeekBar mSeekBar;
    View mLockScreenView;
    Point windowPoint;
    int mCurCameraFace = Camera.CameraInfo.CAMERA_FACING_BACK;
    long lastTime;
    int lastDistance;
    boolean isScreenLock;

    Runnable hiddenRunnable = new Runnable() {
        @Override
        public void run() {
            mSeekBar.setVisibility(GONE);
        }
    };

    private void initView(Context context, AttributeSet attrs) {
        addView(mInnerView = new CameraView(context));
        addView(mFocusView = new TouchDrawView(context));
        windowPoint = new Point();
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        Context context = getContext();
        addView(mLockScreenView = new View(context));

        int bright = getActivityBrightness();
        Log.d("tag", "current bright : " + bright);
        mSeekBar = new SeekBar(context);
        mSeekBar.setMax(255);
        mSeekBar.setProgress(bright);
        mSeekBar.setVisibility(GONE);
        mSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                changeAppBrightness(progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                removeCallbacks(hiddenRunnable);
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                postDelayed(hiddenRunnable, SHOW_TIME);
            }
        });
        int barwidth = AppUtils.dp2px(context, 30);
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(-1, barwidth);
        params.gravity = Gravity.CENTER;
        addView(mSeekBar, params);

        //将seekbar 变成竖的，并靠边显示
        int width = getResources().getDisplayMetrics().widthPixels / 2;
        mSeekBar.animate().rotation(-90).translationX(width - barwidth).setDuration(1);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int count = event.getPointerCount();
        if(count == 1) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN :
                    lastTime = System.currentTimeMillis();
                    float x = event.getX(), y = event.getY();
                    mFocusView.invalidate(x, y);
                    mInnerView.focusOnTouch(x, y);
                    break;
                case MotionEvent.ACTION_UP :
                    if((System.currentTimeMillis() - lastTime > 2000)) {
                        showSeekBar();
                    }
                    lastTime = Long.MAX_VALUE;
                    break;
                case MotionEvent.ACTION_CANCEL :
                    lastTime = Long.MAX_VALUE;
                    break;
            }
            return true;
        } else if(count >= 2) { //多个手指调焦
            switch (event.getActionMasked()) {
                case MotionEvent.ACTION_POINTER_DOWN:
                    lastDistance = distance(event);
                    lastTime = System.currentTimeMillis();
                    break;
                case MotionEvent.ACTION_MOVE:
                    long time = System.currentTimeMillis();
                    if (time - lastTime > 50) { //防止调焦过快
                        sendFocusChange(distance(event));
                        lastTime = time;
                    }
                    break;
                case MotionEvent.ACTION_POINTER_UP:
                    sendFocusChange(distance(event));
                    break;
            }
            return true;
        }
        return super.onTouchEvent(event);
    }

    private void sendFocusChange(int dis) {
        boolean up = dis > lastDistance;
        int d = Math.abs(dis - lastDistance);
        if(d > 10) { //移动的距离超过设定的值才能调焦
            lastDistance = dis;
            mInnerView.changeZoom(up);
        }
    }

    private int distance(MotionEvent event) {
        float x1 = event.getX(0);
        float x2 = event.getX(1);
        float y1 = event.getY(0);
        float y2 = event.getY(1);

        double _x = (x1- x2);
        double _y = (y1 - y2);
        return (int) Math.sqrt(_x*_x+_y*_y);
    }

    private class CameraView extends SurfaceView implements SurfaceHolder.Callback, Camera.AutoFocusCallback {

        public CameraView(Context context) {
            super(context);
            initView(context, null);
        }

        private Camera camera;//这个是hardare的Camera对象
        private int stepCount;
        private boolean isPreviewActive=true;//preview是否是活动的。防止preview是inactive时去调用对焦产生异常。

        private void initView(Context context, AttributeSet attrs) {
            SurfaceHolder holder = getHolder();
            //为了实现照片预览功能，需要将SurfaceHolder的类型设置为PUSH
            //这样，画图缓存就由Camera类来管理，画图缓存是独立于Surface的
            holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
            holder.addCallback(this);

            int widthPixels = getResources().getDisplayMetrics().widthPixels;
            //之前用dpi不管用，现在改成用像素来
            if(widthPixels >= 1080) stepCount = 4;
            else if(widthPixels >= 720) stepCount = 3;
            else if(widthPixels >= 480) stepCount = 2;
            else stepCount = 1;
            Logger.d("widthPixels :" , widthPixels , ",step count:" ,stepCount);
        }

        @Override
        public void onAutoFocus(boolean success, Camera camera) {}

        @Override
        public void surfaceCreated(SurfaceHolder holder) {
            Logger.w("surfaceCreated");
            switchCamera(mCurCameraFace, holder);
        }

        @Override
        public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            Logger.d("surfaceChanged", width, height);
            windowPoint.set(width, height);
        }

        @Override
        public void surfaceDestroyed(SurfaceHolder holder) {
            if(camera != null) {
                camera.stopPreview();
                camera.release();
            }
            camera = null;
            Logger.w("surfaceDestroyed");
        }

        public void switchCamera(int cameraFace, SurfaceHolder surfaceHolder) {
            Logger.w("switchCamera", cameraFace);
            if(surfaceHolder == null) {
                Logger.e("surfaceHolder is null");
                return;
            }
            try {
                if(cameraFace != mCurCameraFace) {
                    camera.stopPreview();
                    camera.release();
                    camera = null;
                }
                //查询是否有设定的摄像头，没有的话就用默认的方式
                Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
                int cameraCount = getNumberOfCameras();
                for(int camIdx = 0;camIdx < cameraCount;camIdx++ ){
                    Camera.getCameraInfo(camIdx,cameraInfo);
                    if(cameraInfo.facing == cameraFace){
                        camera = Camera.open(camIdx);
                        break;
                    }
                }
                if(camera == null) {
                    camera = Camera.open();
                }
            } catch (Exception e) {
                Logger.e(e);
                Snackbar.make(this, "摄像头启动失败，请检测是否有开启相机权限。" + e.getMessage(), Snackbar.LENGTH_LONG).show();
                return;
            }

            /**
             * Camera对象中含有一个内部类Camera.Parameters.该类可以对Camera的特性进行定制
             * 在Parameters中设置完成后，需要调用Camera.setParameters()方法，相应的设置才会生效
             * 由于不同的设备，Camera的特性是不同的，所以在设置时，需要首先判断设备对应的特性，再加以设置
             * 比如在调用setEffects之前最好先调用getSupportedColorEffects。如果设备不支持颜色特性，那么该方法将
             * 返回一个null
             */
//            if(getResources().getConfiguration().orientation != Configuration.ORIENTATION_LANDSCAPE) {
                //如果是竖屏
                camera.setDisplayOrientation(90);
//            }else{
//                camera.setDisplayOrientation(180);
//            }
            try {
                camera.setPreviewDisplay(surfaceHolder);
                //启动预览功能
                setCameraParam();
                camera.startPreview();
                isPreviewActive = true;
            } catch (Exception e) {
                Logger.e(e);
                // 如果出现异常，则释放Camera对象
                camera.release();
                camera = null;
            }
        }

        private boolean changeZoom(boolean up) {
            if(camera == null) return false;

            Camera.Parameters parameters = camera.getParameters();
            if(parameters.isZoomSupported()) {
                int i = parameters.getZoom(), j = i;
                if (up) {
                    i += stepCount;
                    int max = parameters.getMaxZoom();
                    if(i > max) {
                        i = max;
                    }
                } else {
                    i -= stepCount;
                    if(i < 0) i = 0;
                }
                Log.d("camera zoom", String.valueOf(i));
                if(j != i) {
                    parameters.setZoom(i);
                    camera.setParameters(parameters);
                }
                return true;
            }
            return false;
        }

        private void setCameraParam() {
            if(camera == null) return;
            DisplayMetrics dm = getResources().getDisplayMetrics();
            float rate = 1.0f * dm.heightPixels / dm.widthPixels; //屏幕的宽高比
            int height = 2200;

            Camera.Parameters param = camera.getParameters();
            param.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
            param.setPictureFormat(ImageFormat.JPEG);
            Camera.Size p = getFitSize(param.getSupportedPictureSizes(), height, rate);
            if (p != null) {
                param.setPictureSize(p.width, p.height);
                Logger.d("picture size:", p.width, p.height);
            }
            //屏幕预览只能选择跟手机屏幕接近的大小，才不会变形
            Camera.Size p2 = getFitSize(param.getSupportedPreviewSizes(), dm.heightPixels, rate);
            if (p2 != null) {
                param.setPreviewSize(p2.width, p2.height);
                Logger.d("preview size:", p2.width, p2.height);
            }
            camera.setParameters(param);
            if (param.isZoomSupported()) {
                Logger.d("max zoom", param.getMaxZoom());
            }
        }

        /**
         *  获取相机最适合使用的分辨率
         */
        private Camera.Size getFitSize(List<Camera.Size> sizeList, int h, float rate) {
            //size的width和height是横屏状态的高宽，竖屏下需要对换下
            int width = h;

            int offset = Integer.MAX_VALUE;
            Camera.Size ss = null;
            for(Camera.Size size : sizeList) {
                if(size.width >= width && equalRate(size, rate)) {
                    if((size.width - width) < offset) {
                        offset = size.width - width;
                        ss = size;
                    }
                }
            }
            return ss;
        }

        private boolean equalRate(Camera.Size s, float rate) {
            float r = (float) (s.width) / (float) (s.height);
            if (Math.abs(r - rate) <= 0.2) {
                return true;
            } else {
                return false;
            }
        }

        public void focusOnTouch(float x, float y) {
            if(isPreviewActive == false || camera == null) return;
            Rect focusRect = calculateTapArea(windowPoint,x, y, 1f);
            Rect meteringRect = calculateTapArea(windowPoint,x , y, 1.5f);

            Camera.Parameters parameters = camera.getParameters();
            if (parameters.getMaxNumFocusAreas() > 0) {
                List<Camera.Area> focusAreas = new ArrayList<Camera.Area>();
                focusAreas.add(new Camera.Area(focusRect, 1000));
                parameters.setFocusAreas(focusAreas);
            }
            if (parameters.getMaxNumMeteringAreas() > 0) {
                List<Camera.Area> meteringAreas = new ArrayList<Camera.Area>();
                meteringAreas.add(new Camera.Area(meteringRect, 1000));
                parameters.setMeteringAreas(meteringAreas);
            }
            camera.setParameters(parameters);
            camera.autoFocus(this);
        }
        private Rect calculateTapArea(Point point, float rawX, float rawY, float coefficient) {
            float focusAreaSize = 300;
            int areaSize = Float.valueOf(focusAreaSize * coefficient).intValue();
            int centerX = (int) (rawX / point.x * 2000 - 1000);
            int centerY = (int) (rawY / point.y * 2000 - 1000);

            int left = clamp(centerX - areaSize / 2, -1000, 1000);
            int right = clamp(left + areaSize, -1000, 1000);
            int top = clamp(centerY - areaSize / 2, -1000, 1000);
            int bottom = clamp(top + areaSize, -1000, 1000);

            return new Rect(left, top, right, bottom);
        }
        private int clamp(int x, int min, int max) {
            return x >= max ? max : (x < min ? min : x);
        }

        public void takePicture(Camera.PictureCallback callback) {
            if(camera == null && isPreviewActive == false) return;
            isPreviewActive = false;
            camera.takePicture(null, null, callback);
        }

        public void startPreview() {
            if(camera == null) return;
            camera.stopPreview();
            camera.startPreview();
            isPreviewActive = true;
        }
    }

    private class TouchDrawView extends View {
        int X; // X坐标
        int Y; // Y坐标
        int radius; // 半径
        Paint paint; // 画笔
        static final int B = 120;//防止画出来的圆，四边不整齐

        public TouchDrawView(Context context) {
            super(context);
            paint = initPaint();
        }

        @Override
        protected void onDraw(Canvas canvas) {
            if(radius == 0) return;
           canvas.drawCircle(X, Y, radius, paint);
           if(radius == 100) {
               radius = 80;
               postInvalidateDelayed(100,
                       X - B, Y - B, X + B, Y + B);
           } else if(radius == 80) {
               radius = 0;
               postInvalidateDelayed(1000);
           }
        }
        public void invalidate(float x, float y) {
            radius = 100;
            this.X = (int) x;
            this.Y = (int) y;
            invalidate(X - 120, Y - 120, X + 120, Y + 120);
        }
        private Paint initPaint() {
            Paint paint = new Paint();
            paint.setAntiAlias(true);// 抗锯齿
            paint.setStrokeWidth(6);// 描边宽度
            paint.setStyle(Paint.Style.STROKE);// 圆环
            paint.setAlpha(255 - 80);// 透明度
            paint.setColor(Color.WHITE);// 颜色
            return paint;
        }
    }

    public int getNumberOfCameras() {
        return Camera.getNumberOfCameras();
    }

    /**
     * 切换前后摄像头
     */
    public void switchCamera() {
        int i;
        if(mCurCameraFace == Camera.CameraInfo.CAMERA_FACING_BACK) {
            i = Camera.CameraInfo.CAMERA_FACING_FRONT;
        } else {
            i = Camera.CameraInfo.CAMERA_FACING_BACK;
        }
        mInnerView.switchCamera(i, mInnerView.getHolder());
        mCurCameraFace = i;
    }

    public void takePicture(Camera.PictureCallback callback) {
        mInnerView.takePicture(callback);
    }

    public void startPreview() {
        mInnerView.startPreview();
    }

    public boolean isScreenLock() {
        return isScreenLock;
    }

    public void toggleLock() {
        isScreenLock = !isScreenLock;
        if(isScreenLock) {
            Toast.makeText(getContext(), "您开启了锁屏功能", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getContext(), "您关闭了锁屏功能", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * 改变App当前Window亮度
     *
     * @param brightness
     */
    private void changeAppBrightness(@IntRange(from = 0, to = 255)int brightness) {
        if(isScreenLock) return;

        Activity activity = (Activity) getContext();
        Window window = activity.getWindow();
        WindowManager.LayoutParams lp = window.getAttributes();
        if (brightness == -1) {
            lp.screenBrightness = WindowManager.LayoutParams.BRIGHTNESS_OVERRIDE_NONE;
        } else {
            lp.screenBrightness = (brightness <= 0 ? 1 : brightness) / 255f;
        }
        window.setAttributes(lp);
        mLockScreenView.setBackgroundColor(Color.argb(255 - brightness, 0, 0,0));
    }

    private int getActivityBrightness() {
        int screenBrightness = -1;
        try {
            screenBrightness = Settings.System.getInt(getContext().getContentResolver(),
                    Settings.System.SCREEN_BRIGHTNESS);
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        return screenBrightness;
    }
    private void showSeekBar() {
        if(mSeekBar.getVisibility() == VISIBLE) {
            removeCallbacks(hiddenRunnable);
        } else {
            mSeekBar.setVisibility(VISIBLE);
        }
        postDelayed(hiddenRunnable, SHOW_TIME);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if(isScreenLock) return true;
        return super.dispatchTouchEvent(ev);
    }

    public void autoFocus(Camera.AutoFocusCallback callback) {
        if(mInnerView.isPreviewActive && mInnerView.camera != null) {
            mInnerView.camera.autoFocus(callback);
        }
    }

    public boolean isCameraFacingBack() {
        return mCurCameraFace == Camera.CameraInfo.CAMERA_FACING_BACK;
    }
}
