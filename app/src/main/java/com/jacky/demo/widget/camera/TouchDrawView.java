package com.jacky.demo.widget.camera;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by lixinquan on 2020/4/13.
 */
public class TouchDrawView extends View {
    int X; // X坐标
    int Y; // Y坐标
    int radius; // 半径
    Paint paint; // 画笔

    public TouchDrawView(Context context) {
        super(context);
        initView(context);
    }

    public TouchDrawView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public TouchDrawView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    private void initView(Context context) {
        paint = initPaint();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        if(radius == 0) return;
        canvas.drawCircle(X, Y, radius, paint);
        if(radius == 100) {
            radius = 85;
            postInvalidateDelayed(150);
        } else if(radius == 85) {
            radius = 70;
            postInvalidateDelayed(150);
        } else if(radius == 70) {
            radius = 0;
            postInvalidateDelayed(800);
        }
    }
    public void invalidate(float x, float y) {
        radius = 100;
        this.X = (int) x;
        this.Y = (int) y;
        invalidate(X - 120, Y - 120, X + 120, Y + 120);
    }
    private Paint initPaint() {
        Paint paint = new Paint();
        paint.setAntiAlias(true);// 抗锯齿
        paint.setStrokeWidth(6);// 描边宽度
        paint.setStyle(Paint.Style.STROKE);// 圆环
        paint.setAlpha(255 - 80);// 透明度
        paint.setColor(Color.WHITE);// 颜色
        return paint;
    }
    public boolean isDoubleTap(float x, float y) {
        if(radius < 70) return false;
        return (Math.abs(x - X) < radius && Math.abs(y - Y) < radius);
    }
}