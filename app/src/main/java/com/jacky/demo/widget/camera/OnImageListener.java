package com.jacky.demo.widget.camera;

import android.support.annotation.UiThread;

/**
 * Created by lixinquan on 2020/4/10.
 */
public interface OnImageListener {

    @UiThread
    void onImage(byte[] data, int rotation);
}
