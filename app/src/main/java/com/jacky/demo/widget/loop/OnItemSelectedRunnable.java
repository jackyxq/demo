package com.jacky.demo.widget.loop;

/**
 * Created by Administrator on 2017-08-17.
 */

final class OnItemSelectedRunnable implements Runnable {
    final LoopView loopView;

    OnItemSelectedRunnable(LoopView loopview) {
        loopView = loopview;
    }

    @Override
    public final void run() {
        loopView.onItemSelectedListener.onItemSelected(loopView.getSelectedItemPosition(), loopView.getSelectedItem());
    }
}
