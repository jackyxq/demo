package com.jacky.demo.widget.loop;

/**
 * Created by Administrator on 2017-08-17.
 */

public interface OnItemSelectedListener {
    void onItemSelected(int index, LoopText text);
}