package com.jacky.demo.widget;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;

/**
 * 将视图按梅花的布局分开展示
 */

public class PlumLayout extends ViewGroup {

    public PlumLayout(Context context) {
        super(context);
        initView(context, null);
    }

    public PlumLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context, attrs);
    }

    public PlumLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context, attrs);
    }

    private OnItemClickListener mListener;

    private void initView(Context context, AttributeSet attrs) {}

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        if(getChildCount() == 0) {
            setMeasuredDimension(0, 0);
            return;
        }

        int wMode = MeasureSpec.getMode(widthMeasureSpec);
        int wSize = MeasureSpec.getSize(widthMeasureSpec);
        int hMode = MeasureSpec.getMode(heightMeasureSpec);
        int hSize = MeasureSpec.getSize(heightMeasureSpec);

        View view = getChildAt(0);
        if(wMode != MeasureSpec.EXACTLY) {
            int w = view.getWidth() * 3;
            if(wMode == MeasureSpec.AT_MOST) {
                wSize = Math.min(w, wSize);
            } else {
                wSize = w;
            }
        }
        if(hMode != MeasureSpec.EXACTLY) {
            int w = view.getHeight() * 3;
            if(hMode == MeasureSpec.AT_MOST) {
                hSize = Math.min(w, wSize);
            } else {
                hSize = w;
            }
        }
        int size = hSize == 0 ? wSize : Math.min(wSize, hSize);
        measureChildrenView(size);
        //将该布局设置为一个正方形
        setMeasuredDimension(size, size);
    }

    private void measureChildrenView(int width) {
        int w = width / 6;
        int spec = MeasureSpec.makeMeasureSpec(w, MeasureSpec.EXACTLY);

        for(int i = 0, size = getChildCount();i < size; i++) {
            View view = getChildAt(i);
            if(i == 0) {
                int s = MeasureSpec.makeMeasureSpec(w * 2, MeasureSpec.EXACTLY);
                view.measure(s, s);
            } else {
                view.measure(spec, spec);
            }
        }
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        int size = getChildCount();
        if(size == 0) return;

        //设置中间按钮的位置
        View centerView = getChildAt(0);
        int width = r - l;
        int left = width / 3, right = left * 2;
        centerView.layout(left, left, right, right);

        //周边按钮根据梅花布局的位置
        int top, radio = width * 5 /12, viewSize = width / 6;
        int offset = (width - viewSize ) / 2; //按钮的左上角的位置
        double alpha = Math.PI * 2 / (size - 1), angel;
        for(int i = 1;i < size;i++) {
            angel = alpha * (i - 1);
            View view = getChildAt(i);

            double x = Math.sin(angel);
            double y = Math.cos(angel);
            left = (int) (radio * x) + offset;
            top = offset - (int) (radio * y);

            view.layout(left, top, left + viewSize, top + viewSize);
        }
    }

    public void setAdapter(PlumAdapter adapter) {
        if(adapter == null) return;
        int oldSize = getChildCount();
        View[] mOldView = new View[oldSize];
        for(int i = 0;i < oldSize;i++ ) {
            mOldView[i] = getChildAt(i);
        }

        removeAllViews();
        View mCenterView = oldSize == 0 ? null : mOldView[0];
        oldSize--; //扣除中间的VIEW

        int size = adapter.getCount();
        mCenterView = adapter.getCenterView(mCenterView);
        //reuse old view
        View[] around = new View[size];
        for(int i = 0;i < size;i++) {
            around[i] = adapter.getAroundView(i < oldSize ? mOldView[i + 1] : null, i);
        }

        addView(mCenterView);
        for(View v : around) {
            addView(v);
        }
        invalidate();
    }

    private long clicktime;

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        return mListener == null ? false : true;
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        switch (ev.getAction()) {
            case MotionEvent.ACTION_DOWN :
                clicktime = System.currentTimeMillis();
                break;
            case MotionEvent.ACTION_UP :
                long now = System.currentTimeMillis();
                if(now - clicktime <= ViewConfiguration.getJumpTapTimeout()){
                    itemClick((int)ev.getX(), (int)ev.getY());
                }
                break;
        }
        return true;
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        mListener = listener;
    }

    public interface PlumAdapter {
        int getCount();
        View getCenterView(View view);
        View getAroundView(View view, int position);
    }

    public interface OnItemClickListener {
        void onItemClick(PlumLayout parent, View view, int position);
    }

    private void itemClick(int x, int y) {
        Rect temp = new Rect();
        for(int i = 0, size = getChildCount();i < size;i++ ) {
            View view = getChildAt(i);
            view.getHitRect(temp);
            if(temp.contains(x, y)) {
                mListener.onItemClick(this, view, i);
                return;
            }
//            Logger.e(temp, x, y);
        }

    }

}
