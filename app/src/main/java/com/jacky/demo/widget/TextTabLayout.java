package com.jacky.demo.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.StateListDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * 将选中的Indicator只显示在字体下面
 */
public class TextTabLayout extends TabLayout {

    public TextTabLayout(Context context) {
        super(context);
        init(context, null);
    }

    public TextTabLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public TextTabLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private int tabIndicatorColor;
    private int tabIndicatorHeight;

    private void init(Context context, AttributeSet attrs) {
        TypedArray a = context.obtainStyledAttributes(attrs, android.support.design.R.styleable.TabLayout);
        tabIndicatorColor = a.getColor(android.support.design.R.styleable.TabLayout_tabIndicatorColor, 0xff333333);
        tabIndicatorHeight = a.getDimensionPixelSize(android.support.design.R.styleable.TabLayout_tabIndicatorHeight, 10);
        a.recycle();

        setSelectedTabIndicatorHeight(0);//不展示原来的选中颜色
    }

    @Override
    public void addTab(@NonNull Tab tab, int position, boolean setSelected) {
        super.addTab(tab, position, setSelected);

        Context context = getContext();
        FrameLayout layout = new FrameLayout(context);
        layout.setLayoutParams(new FrameLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));

        TextView view = new TextView(context);
        view.setId(android.R.id.text1);
        view.setTextColor(getTabTextColors());
        view.setTextSize(12);
        view.setText(tab.getText());
        FrameLayout.LayoutParams p = new LayoutParams(-2, -2);
        p.gravity = Gravity.CENTER;
        p.topMargin = p.bottomMargin = tabIndicatorHeight * 2;
        view.setLayoutParams(p);
        layout.addView(view);

        Paint paint = view.getPaint();
        int width = (int) paint.measureText(tab.getText().toString());
        p = new LayoutParams(width, tabIndicatorHeight);
        p.gravity = Gravity.BOTTOM;

        ImageView icon = new ImageView(context);
        icon.setBackground(getIndicatorDrawable());
        icon.setLayoutParams(p);
        layout.addView(icon);

        tab.setCustomView(layout);
    }

    public void relayout(Tab tab) {
        if(tab == null) return;
        View view = tab.getCustomView();
        if(view == null) return;
        ViewGroup group = (ViewGroup) view;
        ImageView icon = (ImageView) group.getChildAt(1);
        FrameLayout.LayoutParams p = (LayoutParams) icon.getLayoutParams();

        Paint paint = ((TextView) group.getChildAt(0)).getPaint();
        int width = (int) paint.measureText(tab.getText().toString());
        if(width > 0) p.width = width;
    }

//    @Override
//    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
//        for(int i = 0, size = getTabCount();i < size;i++) {
//            ViewGroup view = (ViewGroup) getTabAt(i).getCustomView();
//            View tv = view.getChildAt(0);
//            View iv = view.getChildAt(1);
//            Logger.e(i, tv.getMeasuredWidth());
//
//            LayoutParams p = (LayoutParams) iv.getLayoutParams();
//            p.width = tv.getMeasuredWidth();
//            iv.setLayoutParams(p);
//        }
//        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
//    }

    private Drawable getIndicatorDrawable() {
        //使用圆角的横线
        int r = 10;
        float[] outerR = new float[] { r, r, r, r, r, r, r, r };
        RoundRectShape ovalShape = new RoundRectShape(outerR, null, null);
        ShapeDrawable drawable1 = new ShapeDrawable(ovalShape);
        drawable1.setIntrinsicHeight(tabIndicatorHeight);
        drawable1.setIntrinsicWidth(200);
        drawable1.getPaint().setColor(tabIndicatorColor);
        drawable1.getPaint().setStyle(Paint.Style.FILL);

        StateListDrawable drawable = new StateListDrawable();
        drawable.addState(new int[]{android.R.attr.state_selected}, drawable1);
        return drawable;
    }
}
