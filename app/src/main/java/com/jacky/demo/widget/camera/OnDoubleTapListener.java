package com.jacky.demo.widget.camera;

/**
 * Created by lixinquan on 2020/4/13.
 */
public interface OnDoubleTapListener {
    void onDoubleTap();
}
