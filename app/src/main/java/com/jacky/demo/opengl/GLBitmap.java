package com.jacky.demo.opengl;

import android.graphics.Bitmap;
import android.opengl.GLES20;
import android.opengl.GLUtils;

import java.nio.FloatBuffer;

import javax.microedition.khronos.opengles.GL10;


/**
 * Created by lixinquan on 2019/2/14.
 */
public class GLBitmap extends Shape {

    private FloatBuffer vertexBuffer, textureBuffer; // buffer holding the vertices
    private int[] textures = new int[1];
    int avPosition, afPosition;

    GLBitmap(Bitmap bm) {
        vertexBuffer = GlUtils.allocateFloatBuffer(GlUtils.vertexData);
        textureBuffer = GlUtils.allocateFloatBuffer(GlUtils.textureData);

        loadGLTexture(bm);
        afPosition = GLES20.glGetAttribLocation(mProgram, "af_Position");
        avPosition = GLES20.glGetAttribLocation(mProgram, "av_Position");
    }

    @Override
    void onSurfaceChanged(GL10 gl, int width, int height) {
        GLES20.glViewport(200, 100, 400, 400);
    }

    @Override
    void draw() {
        GLES20.glEnable(GLES20.GL_BLEND);
        GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);
        GLES20.glUseProgram(mProgram);

        GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D,  textures[0]);//把已经处理好的Texture传到GL上面

        GLES20.glEnableVertexAttribArray(afPosition);
        GLES20.glEnableVertexAttribArray(avPosition);

        GLES20.glVertexAttribPointer(avPosition, 2, GLES20.GL_FLOAT, false, 8, vertexBuffer);
        GLES20.glVertexAttribPointer(afPosition, 2, GLES20.GL_FLOAT, false, 8, textureBuffer);

        GLES20.glDrawArrays(GLES20.GL_TRIANGLE_STRIP, 0, 4);
        GLES20.glDisableVertexAttribArray(afPosition);
        GLES20.glDisableVertexAttribArray(avPosition);
        GLES20.glDisable(GLES20.GL_BLEND);
    }

    private void loadGLTexture(Bitmap bitmap) {
        if(bitmap == null || bitmap.isRecycled()) return;

        // generate one texture pointer
        GLES20.glGenTextures(1,textures,0);
        //生成纹理
         GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textures[0]);
         //设置缩小过滤为使用纹理中坐标最接近的一个像素的颜色作为需要绘制的像素颜色
         GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER,GLES20.GL_NEAREST);
         //设置放大过滤为使用纹理中坐标最接近的若干个颜色，通过加权平均算法得到需要绘制的像素颜色
         GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D,GLES20.GL_TEXTURE_MAG_FILTER,GLES20.GL_LINEAR);
         //设置环绕方向S，截取纹理坐标到[1/2n,1-1/2n]。将导致永远不会与border融合
         GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S,GLES20.GL_CLAMP_TO_EDGE);
         //设置环绕方向T，截取纹理坐标到[1/2n,1-1/2n]。将导致永远不会与border融合
         GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T,GLES20.GL_CLAMP_TO_EDGE);
         //根据以上指定的参数，生成一个2D纹理
         GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, bitmap, 0);
    }

    @Override
    protected String getVertexShader() {
        return GlUtils.waterVertexShader;
    }

    @Override
    protected String getFragmentShader() {
        return GlUtils.waterFragmentShader;
    }
}
