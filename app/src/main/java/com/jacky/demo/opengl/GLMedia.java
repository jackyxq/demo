package com.jacky.demo.opengl;

import android.opengl.GLES20;

import javax.microedition.khronos.opengles.GL10;

/**
 * Created by lixinquan on 2019/2/22.
 */
public class GLMedia extends Shape {

    int textureID;
//    private static final String VERTEX_SHADER =
//            "uniform mat4 uMVPMatrix;\n" +
//                    "uniform mat4 uTexMatrix;\n" +
//                    "attribute vec4 aPosition;\n" +
//                    "attribute vec4 aTextureCoord;\n" +
//                    "varying vec2 vTextureCoord;\n" +
//                    "void main() {\n" +
//                    "    gl_Position = uMVPMatrix * aPosition;\n" +
//                    "    vTextureCoord = (uTexMatrix * aTextureCoord).xy;\n" +
//                    "}\n";
//    // Simple fragment shader for use with external 2D textures (e.g. what we get from
//    // SurfaceTexture).
//    private static final String FRAGMENT_SHADER_EXT =
//            "#extension GL_OES_EGL_image_external : require\n" +
//                    "precision mediump float;\n" +
//                    "varying vec2 vTextureCoord;\n" +
//                    "uniform samplerExternalOES sTexture;\n" +
//                    "void main() {\n" +
//                    "    gl_FragColor = texture2D(sTexture, vTextureCoord);\n" +
//                    "}\n";

    @Override
    public String getVertexShader() {
        return GlUtils.waterVertexShader;
    }

    @Override
    protected String getFragmentShader() {
        return GlUtils.waterFragmentShader;
    }

//    private static final float FULL_RECTANGLE_COORDS[] = {
//            -1.0f, -1.0f,   // 0 bottom left
//            1.0f, -1.0f,   // 1 bottom right
//            -1.0f,  1.0f,   // 2 top left
//            1.0f,  1.0f,   // 3 top right
//    };
//    private static final float FULL_RECTANGLE_TEX_COORDS[] = {
//            0.0f, 0.0f,     // 0 bottom left
//            1.0f, 0.0f,     // 1 bottom right
//            0.0f, 1.0f,     // 2 top left
//            1.0f, 1.0f      // 3 top right
//    };
//    private static final FloatBuffer FULL_RECTANGLE_BUF = allocateFloatBuffer(FULL_RECTANGLE_COORDS);
//    private static final FloatBuffer FULL_RECTANGLE_TEX_BUF = allocateFloatBuffer(FULL_RECTANGLE_TEX_COORDS);

    // Handles to the GL program and various components of it.
//    private int muMVPMatrixLoc;
//    private int muTexMatrixLoc;
//    private int maPositionLoc;
//    private int maTextureCoordLoc;
    int avPosition, afPosition;

    GLMedia() {
//        int p = mProgram;
//        maPositionLoc = GLES20.glGetAttribLocation(p, "aPosition");
//        maTextureCoordLoc = GLES20.glGetAttribLocation(p, "aTextureCoord");
//        muMVPMatrixLoc = GLES20.glGetUniformLocation(p, "uMVPMatrix");
//        muTexMatrixLoc = GLES20.glGetUniformLocation(p, "uTexMatrix");
        afPosition = GLES20.glGetAttribLocation(mProgram, "af_Position");
        avPosition = GLES20.glGetAttribLocation(mProgram, "av_Position");
    }

    void setTextureID(int id, float[] f) {
        textureID = id;
//        transform = f;
    }

    @Override
    void onSurfaceChanged(GL10 gl, int width, int height) {

    }
//    float[] transform;

    @Override
    void draw() {
//        draw(new float[]{
//                1,0,0,0,
//                0,1,0,0,
//                0,0,1,0,
//                0,0,0,1
//        });
        drawFrame();

//        drawBox(mFrameNum++);
    }

//    int mFrameNum;

//    private void drawBox(int posn) {
//        final int width = 1900;
//        int xpos = (posn * 4) % (width - 50);
//        GLES20.glEnable(GLES20.GL_SCISSOR_TEST);
//        GLES20.glScissor(xpos, 0, 100, 100);
//        GLES20.glClearColor(1.0f, 0.0f, 1.0f, 1.0f);
//        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT);
//        GLES20.glDisable(GLES20.GL_SCISSOR_TEST);
//    }

    private void drawFrame() {
        GLES20.glDisable(GLES20.GL_DEPTH_TEST);
//        GLES20.glEnable(GLES20.GL_BLEND);
//        GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);
        GLES20.glUseProgram(mProgram);

        GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D,  textureID);//把已经处理好的Texture传到GL上面

        GLES20.glEnableVertexAttribArray(afPosition);
        GLES20.glEnableVertexAttribArray(avPosition);

        GLES20.glVertexAttribPointer(avPosition, 2, GLES20.GL_FLOAT, false, 8, GlUtils.vertexBuffer);
        GLES20.glVertexAttribPointer(afPosition, 2, GLES20.GL_FLOAT, false, 8, GlUtils.textureBuffer);

        GLES20.glDrawArrays(GLES20.GL_TRIANGLE_STRIP, 0, 4);
        GLES20.glDisableVertexAttribArray(afPosition);
        GLES20.glDisableVertexAttribArray(avPosition);
//        GLES20.glDisable(GLES20.GL_BLEND);
    }

    private void draw(float[] mvpMatrix) {

        // Select the program.
//        GLES20.glUseProgram(mProgram);
//
//        // Set the texture.
//        GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
//        GLES20.glBindTexture(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, textureID);
//
//        // Copy the model / view / projection matrix over.
//        GLES20.glUniformMatrix4fv(muMVPMatrixLoc, 1, false, mvpMatrix, 0);
//
//        // Copy the texture transformation matrix over.
//        GLES20.glUniformMatrix4fv(muTexMatrixLoc, 1, false, transform, 0);
//
//        // Enable the "aPosition" vertex attribute.
//        GLES20.glEnableVertexAttribArray(maPositionLoc);
//
//        // Connect vertexBuffer to "aPosition".
//        GLES20.glVertexAttribPointer(maPositionLoc, 2,
//                GLES20.GL_FLOAT, false, 8, FULL_RECTANGLE_BUF);
//
//        // Enable the "aTextureCoord" vertex attribute.
//        GLES20.glEnableVertexAttribArray(maTextureCoordLoc);
//
//        // Connect texBuffer to "aTextureCoord".
//        GLES20.glVertexAttribPointer(maTextureCoordLoc, 2,
//                GLES20.GL_FLOAT, false, 8, FULL_RECTANGLE_TEX_BUF);
//
//        // Draw the rect.
//        GLES20.glDrawArrays(GLES20.GL_TRIANGLE_STRIP, 0, 4);
//
//        // Done -- disable vertex array, texture, and program.
//        GLES20.glDisableVertexAttribArray(maPositionLoc);
//        GLES20.glDisableVertexAttribArray(maTextureCoordLoc);
//        GLES20.glBindTexture(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, 0);
//        GLES20.glUseProgram(0);
    }
}
