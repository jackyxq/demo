package com.jacky.demo.opengl;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.SurfaceTexture;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.os.Build;
import android.util.AttributeSet;

import com.jacky.demo.R;
import com.jacky.log.Logger;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

/**
 * Created by lixinquan on 2019/2/14.
 *
 * 参考项目 ： https://github.com/google/grafika
 */
@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
public class CameraGLSurfaceView extends GLSurfaceView implements GLSurfaceView.Renderer, SurfaceTexture.OnFrameAvailableListener  {

    public CameraGLSurfaceView(Context context) {
        this(context, null);
    }

    public CameraGLSurfaceView(Context context, AttributeSet attrs) {
        super(context, attrs);

        setEGLContextClientVersion(2);
        setRenderer(this);

        setRenderMode(RENDERMODE_WHEN_DIRTY);//主动调用渲染
    }

    GLCamera shape;
    MediaRecorder.VideoEncoder mVideoEncoder;

    public void setMovieRecorder(MediaRecorder.VideoEncoder recorder) {
        mVideoEncoder = recorder;
    }

    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        Logger.w("camera gl surface created...");
        gl.glClearColor(1, 1, 1, 0);
        if(shape == null) {
            Bitmap bm = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
            shape = new GLCamera(bm);
            bm.recycle();
        } else if(mVideoEncoder != null) {
            Logger.i("egl need update context");
            mVideoEncoder.updateCurrentContext();
        }
        shape.onSurfaceCreated(gl, config);
        shape.mSurfaceTextrue.setOnFrameAvailableListener(this);
    }

    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {
        // 设置输出屏幕大小
        shape.onSurfaceChanged(gl, width, height);
    }

    @Override
    public void onDrawFrame(GL10 gl) {
//        Logger.d("on frame draw...");
        // 清除屏幕和深度缓存(如果不调用该代码, 将不显示glClearColor设置的颜色)
        // 同样如果将该代码放到 onSurfaceCreated 中屏幕会一直闪动
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);
        shape.draw();

        if(mVideoEncoder != null) {
            mVideoEncoder.setTextureId(shape.fTexture[0]);
            mVideoEncoder.frameAvailable(shape.mSurfaceTextrue);
        }
    }

    public void pauseRecord() {
        if(shape != null) {
            shape.pause();
        }
        if(mVideoEncoder != null) {
            mVideoEncoder.pause();
        }
    }

    public void release() {
        if(shape != null)
            shape.release();
        shape = null;
    }

    public void resumeRecord() {
        if(shape != null) {
            shape.onSurfaceCreated(null, null);
            shape.mSurfaceTextrue.setOnFrameAvailableListener(this);
        }
        if(mVideoEncoder != null) {
            mVideoEncoder.resume();
        }
    }

    @Override
    public void onFrameAvailable(SurfaceTexture surfaceTexture) {
        requestRender();
    }

    public void switchCamera() {
        shape.switchCamera();
    }

    public void takePicture(PictureCallback callback) {
        shape.takePicture(callback);
    }

    public interface PictureCallback {
        void onPictureTaken(Bitmap bitmap);
    }
    /*package*/int getFitFrameRate(int rate) {
        return shape.getFitFrameRate(rate);
    }
}
