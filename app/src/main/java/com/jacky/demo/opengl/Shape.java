package com.jacky.demo.opengl;

import android.opengl.GLES20;
import android.support.annotation.CallSuper;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

/**
 * Created by lixinquan on 2019/2/14.
 */
public abstract class Shape {

    private static final String DEFAULT_VERTEX_SHADER =
            "attribute vec4 vPosition;" +
            "uniform mat4 vMatrix;" +
            "void main() {" +
            "  gl_Position = vMatrix*vPosition;" +
            "}" ;

    private static final String DEFAULT_FRAGMENT_SHADER =
            "precision mediump float;" +
            " uniform vec4 vColor;" +
            " void main() {" +
            "     gl_FragColor = vColor;" +
            " }";

    // number of coordinates per vertex in this array
    protected static final int COORDS_PER_VERTEX = 3;

    protected int mProgram;

    Shape() {
        mProgram = GlUtils.createProgram(getVertexShader(), getFragmentShader());
    }

    public void onSurfaceCreated(GL10 gl, EGLConfig config) {}
    abstract void onSurfaceChanged(GL10 gl, int width, int height);

    abstract void draw();

    protected String getVertexShader() {
        return DEFAULT_VERTEX_SHADER;
    }

    protected String getFragmentShader() {
        return DEFAULT_FRAGMENT_SHADER;
    }

    @CallSuper
    public void release() {
        GLES20.glDeleteProgram(mProgram);
        mProgram = -1;
    }

}
