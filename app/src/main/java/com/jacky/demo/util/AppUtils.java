package com.jacky.demo.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;

import com.jacky.log.Logger;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

/**
 * Created by Administrator on 2017-06-12.
 */

public final class AppUtils {

    /**
     * 无论成功与否都会回调{@link FragmentActivity#onRequestPermissionsResult(int, String[], int[])}方法
     * @param context
     * @param premission {@link android.Manifest.permission}
     * @param requestCode
     */
    public static void requestPermission(FragmentActivity context, int requestCode, String... premission) {
        if(premission == null) return;
        for(String pre : premission) {
            if (ContextCompat.checkSelfPermission(context, pre) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(context, premission,requestCode);
                return;
            }
        }
        int[] result = new int[premission.length];
        Arrays.fill(result, PackageManager.PERMISSION_GRANTED);
        context.onRequestPermissionsResult(requestCode, premission, result);
    }

    /**
     *  检测权限申请是否可以
     * @param activity
     * @param permission
     * @param grantResults
     * @return
     */
    public static boolean isPermissionOK(Activity activity, String[] permission, int[] grantResults) {
        for(int i : grantResults) {
            if(i == PackageManager.PERMISSION_DENIED) {
                return false;
            }
        }
        return true;
    }

    /**
     * 兼容android 7.0的权限处理
     * @param context
     * @param intent
     * @param file
     * @return
     */
    public static Uri fromFile(Context context, Intent intent, File file) {
        Uri data;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            String authority = context.getPackageName() + ".fileprovider";
            data = FileProvider.getUriForFile(context, authority, file);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        } else {
            data = Uri.fromFile(file);
        }
        return data;
    }

    public static final int dp2px(Context context, float dp) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dp * scale + 0.5f);
    }

    /** 判断手机是否root，不弹出root请求框<br/> */
    public static boolean isRoot() {
        String binPath = "/system/bin/su";
        String xBinPath = "/system/xbin/su";
        if (new File(binPath).exists() && isExecutable(binPath))
            return true;
        if (new File(xBinPath).exists() && isExecutable(xBinPath))
            return true;
        return false;
    }

    private static boolean isExecutable(String filePath) {
        Process p = null;
        try {
            p = Runtime.getRuntime().exec("ls -l " + filePath);
            // 获取返回内容
            BufferedReader in = new BufferedReader(new InputStreamReader(
                    p.getInputStream()));
            String str = in.readLine();
            Logger.i(str);
            if (str != null && str.length() >= 4) {
                char flag = str.charAt(3);
                if (flag == 's' || flag == 'x')
                    return true;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }finally{
            if(p!=null){
                p.destroy();
            }
        }
        return false;
    }
}
