package com.jacky.demo.util;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.drawable.Drawable;
import android.support.annotation.IdRes;
import android.support.annotation.IntDef;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

import com.jacky.log.Logger;

import java.util.List;

/**
 * Created by Administrator on 2017-08-30.
 */

public final class ViewUtils {

    public static final <T extends View> T findView(Activity activity, @IdRes int id) {
        return (T) activity.findViewById(id);
    }

    public static final <T extends View> T findView(View group, @IdRes int id) {
        return (T) group.findViewById(id);
    }

    public static final <T extends View> T findView(Dialog dialog, @IdRes int id) {
        return (T) dialog.findViewById(id);
    }

    public static int getIntentInt(Activity activity) {
        return activity.getIntent().getIntExtra(Intent.EXTRA_INDEX, 0);
    }

    public static int[] getIntentIntArray(Activity activity) {
        return activity.getIntent().getIntArrayExtra(Intent.EXTRA_INDEX);
    }

    public static String getIntentString(Activity activity) {
        return activity.getIntent().getStringExtra(Intent.EXTRA_TEXT);
    }

    public static String[] getIntentStringArray(Activity activity) {
        return activity.getIntent().getStringArrayExtra(Intent.EXTRA_TEXT);
    }

    public static final void startActivity(Context context, Class<? extends Activity> clazz) {
        context.startActivity(getIntent(context, clazz));
    }
    public static final void startActivity(Context context, Class<? extends Activity> clazz,int id) {
        context.startActivity(getIntent(context, clazz, id));
    }
    public static final void startActivity(Context context, Class<? extends Activity> clazz,int[] id) {
        context.startActivity(getIntent(context, clazz, id));
    }
    public static final void startActivity(Context context, Class<? extends Activity> clazz, String... params) {
        context.startActivity(getIntent(context, clazz, params));
    }

    public static void finishActivity(Activity context, Class<? extends Activity> clazz,int id) {
        startActivity(context, clazz, id);
        context.finish();
    }
    public static void finishActivity(Activity context, Class<? extends Activity> clazz,int[] id) {
        startActivity(context, clazz, id);
        context.finish();
    }
    public static void finishActivity(Activity context, Class<? extends Activity> clazz, String... params) {
        startActivity(context, clazz, params);
        context.finish();
    }

    public static void startActivityForResult(Activity context, Class<? extends Activity> clazz,
                                                    int requestCode) {
        context.startActivityForResult(getIntent(context, clazz), requestCode);
    }

    public static void finishActivityForResult(Activity context, Class<? extends Activity> clazz,
                                                     int requestCode) {
        startActivityForResult(context, clazz, requestCode);
        context.finish();
    }

    public static Intent getIntent(Context context,Class<? extends Activity> clazz) {
        return new Intent(context, clazz);
    }

    public static Intent getIntent(Context context,Class<? extends Activity> clazz,int... ids) {
        Intent intent = new Intent(context, clazz);
        if(ids != null && ids.length > 0) {
            if(ids.length == 1) {
                intent.putExtra(Intent.EXTRA_INDEX, ids[0]);
            } else {
                intent.putExtra(Intent.EXTRA_INDEX, ids);
            }
        }
        return intent;
    }

    public static Intent getIntent(Context context, Class<? extends Activity> clazz, String... params) {
        Intent intent = new Intent(context, clazz);
        if(params != null && params.length > 0) {
            if(params.length == 1) {
                intent.putExtra(Intent.EXTRA_TEXT, params[0]);
            } else {
                intent.putExtra(Intent.EXTRA_TEXT, params);
            }
        }
        return intent;
    }

    @IntDef({ModeColorFilter.none, ModeColorFilter.backgroud, ModeColorFilter.image_drawable})
    public @interface ModeColorFilter {
        int none = 0;
        int backgroud = 1;
        int image_drawable = 2;
    }

    public static void addClickColorFilter(View v) {
        addClickColorFilter(v, ModeColorFilter.backgroud);
    }

    public static void addClickColorFilter(View v,@ModeColorFilter int mode) {
        if(v == null) return;
        Drawable drawable = null;
        if(mode == ModeColorFilter.backgroud ) {
            drawable = v.getBackground();
        } else if(mode == ModeColorFilter.image_drawable && v instanceof ImageView) {
            drawable = ((ImageView)v).getDrawable();
        }

        View.OnTouchListener touchListener = getTouchListener(drawable, null, mode);
        if (touchListener == null) return;
        v.setOnTouchListener(touchListener);
    }

    public static View.OnTouchListener getTouchListener(
            final Drawable drawable, final ColorMatrixColorFilter cM, final @ModeColorFilter int mode) {
        if (drawable == null || mode == ModeColorFilter.none) {
            return null;
        }
        View.OnTouchListener mTouchListener = new View.OnTouchListener() {
            private float[] BT_SELECTED = new float[]{
                    1, 0, 0, 0, -50,
                    0, 1, 0, 0, -50,
                    0, 0, 1, 0, -50,
                    0, 0, 0, 1, 0};
            private ColorMatrixColorFilter cM_Selected = new ColorMatrixColorFilter(BT_SELECTED);

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                drawable.setColorFilter(cM == null ? cM_Selected : cM);
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        setDrawablet(v, mode);
                        break;
                    case MotionEvent.ACTION_UP:
                        drawable.clearColorFilter();
                        setDrawablet(v, mode);
                        break;
                    case MotionEvent.ACTION_CANCEL:
                        drawable.clearColorFilter();
                        v.setFocusable(false);
                        v.setFocusableInTouchMode(false);
                        setDrawablet(v, mode);
                        break;
                }
                return false;
            }

            private void setDrawablet(View v, @ModeColorFilter int mode) {
                if (mode == ModeColorFilter.backgroud) {
                    v.setBackgroundDrawable(drawable);
                } else if (mode == ModeColorFilter.image_drawable) {
                    ((ImageView) v).setImageDrawable(drawable);
                }
            }
        };
        return mTouchListener;
    }

    /**
     * 一个Activity界面上有多个Fragment需要切换显示，一个Fragment显示的时候，其他Fragment隐藏
     * @param activity
     * @param fragment
     * @param layoutId
     */
    public static void switchFragment(AppCompatActivity activity, Class<? extends Fragment> fragment, @IdRes int layoutId) {
        String tag = fragment.getSimpleName();
        FragmentManager manager = activity.getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();

        Fragment baseFragment = manager.findFragmentByTag(tag);
        @SuppressLint("RestrictedApi") List<Fragment> list = manager.getFragments();
        if(list != null) {
            for (Fragment f : list) {
                if (f == null) continue;
                if (f == baseFragment) {
                    transaction.show(f);
                } else {
                    transaction.hide(f);
                }
            }
        }
        if (baseFragment == null) {
            try {
                baseFragment = fragment.newInstance();
                transaction.add(layoutId, baseFragment, tag);
            } catch (InstantiationException e) {
                Logger.e("V", "", e);
            } catch (IllegalAccessException e) {
                Logger.e("V", "", e);
            }
        }
        transaction.commitAllowingStateLoss();
    }
}
